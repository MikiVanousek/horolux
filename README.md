# Horolux

Horolux is a Flutter app, that allows you to effortlessly perceive you can keep on sleeping.
Before you wake up, it gradually lights up the display.
You can read more about [why Horolux is useful](https://blog.vanousek.com/horolux),
this README focuses on how it is made instead.

There is a [web version of the app](https://horolux.vanousek.com).
You can get an idea of how the app works there. 

[](./screenshots/2022-05-19-19-50-30.png)

## Architecture

The app has 2 screens. On the settings screen, user picks the time he wakes up and the duration for which 

## License
As the author and copyright holder of the code in the directory `lib`, I apply the GPL 3.0 license to it, as it stands in [LICENSE.md](LICENSE.md).

## Privacy Policy

This app does not collect any user data.

