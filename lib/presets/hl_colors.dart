import 'package:flutter/material.dart';

/// All the predefined colors used in the UI.
class HLColors {
  static const Color primary = Colors.black,
      primaryLight = Color(0xFF222222),
      secondary = Colors.white,
      secondaryDark = Color.fromARGB(255, 60, 60, 60),
      sleepFont = Color.fromARGB(255, 18, 18, 18);

  // Theme passed to change color of the material time picker.
  static final materialTheme = ThemeData(
    timePickerTheme: TimePickerThemeData(
      backgroundColor: primaryLight,
      hourMinuteColor: MaterialStateColor.resolveWith((states) =>
          states.contains(MaterialState.selected) ? secondary : primaryLight),
      hourMinuteTextColor: MaterialStateColor.resolveWith(
        (states) =>
            states.contains(MaterialState.selected) ? primary : secondaryDark,
      ),
    ),
    colorScheme: const ColorScheme.dark(
      primary: secondary,
      secondary: primary,
    ),
  );
}
