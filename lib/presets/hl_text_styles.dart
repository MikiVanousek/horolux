import 'package:flutter/cupertino.dart';
import 'package:horolux/presets/hl_colors.dart';

/// All text styles visible in the UI.
class HLTextstyles {
  static final TextStyle base = const TextStyle(
        color: HLColors.secondary,
        decoration: TextDecoration.none,
        fontFamily: 'Roboto',
        fontWeight: FontWeight.w300,
      ),
      clock = base.copyWith(
        fontSize: 72,
        fontWeight: FontWeight.w800,
      ),
      label = base.copyWith(
        color: HLColors.secondaryDark,
        fontSize: 20,
        fontWeight: FontWeight.w300,
      ),
      labelAccent = label.copyWith(
          color: HLColors.secondary, fontWeight: FontWeight.w500),
      duration = label.copyWith(
        fontSize: 26,
      ),
      durationAccent = labelAccent.copyWith(
        fontSize: 26,
      ),
      display = base.copyWith(fontSize: 72),
      button = base.copyWith(
        color: HLColors.primary,
        fontSize: 32,
        fontWeight: FontWeight.w500,
      );
}
