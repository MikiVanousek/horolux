import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:horolux/clock_screen/clock_screen_bloc.dart';
import 'package:horolux/clock_screen/clock_screen_state.dart';
import 'package:horolux/presets/hl_text_styles.dart';
import 'package:horolux/settings/settings.dart';
import 'package:horolux/settings/settings_screen_bloc.dart';
import 'package:wakelock/wakelock.dart';

/// The UI user sees after clicking the `Start` button in settings screen.
///
/// This widget is stateful, because it needs to set sceenlock and fullscreen mode when it is created,
/// and then turn them off when user leaves the screen.
class ClockScreen extends StatefulWidget {
  const ClockScreen({Key? key}) : super(key: key);

  @override
  State<ClockScreen> createState() => _ClockScreenState();
}

class _ClockScreenState extends State<ClockScreen> {
  @override
  void initState() {
    super.initState();
    // Turn on fullscreen mode.
    // It is broken on some phones, I filed an issue on github.
    WidgetsFlutterBinding.ensureInitialized();
    SystemChrome.setEnabledSystemUIMode(SystemUiMode.immersiveSticky);
    // Prevents the device from falling asleep.
    Wakelock.enable();
  }

  @override
  void dispose() {
    Wakelock.disable();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<SettingsScreenBloc, Settings?>(
      builder: (context, settings) {
        if (settings == null) {
          // This should never happen.
          return Center(
            child: Text(
              'Major internal error! This screen was entered without settings.',
              style: HLTextstyles.labelAccent,
            ),
          );
        }
        return BlocProvider(
          create: (_) => ClockScreenBloc(settings, context),
          child: BlocBuilder<ClockScreenBloc, ClockScreenState>(
              builder: (context, state) => Container(
                    color: state.backgroundColor,
                    child: Stack(
                      children: [
                        Padding(
                          padding: const EdgeInsets.all(32.0),
                          child: Material(
                            color: state.backgroundColor,
                            child: IconButton(
                              onPressed: () async {
                                // End fullscreen before going back to [SettingsScreen], otherwise you can watch it happen.
                                await SystemChrome.setEnabledSystemUIMode(
                                    SystemUiMode.manual,
                                    overlays: SystemUiOverlay.values);
                                Navigator.of(context).pop();
                              },
                              icon: Icon(
                                Icons.settings,
                                color: state.accentColor,
                                size: 32,
                              ),
                            ),
                          ),
                        ),
                        Visibility(
                          visible: settings.showTime,
                          child: Center(
                            child: Column(
                              mainAxisSize: MainAxisSize.min,
                              children: [
                                Text(
                                  state.time,
                                  style: HLTextstyles.clock
                                      .copyWith(color: state.accentColor),
                                ),
                                Text(
                                  state.sleepLeftMessage,
                                  style: HLTextstyles.label
                                      .copyWith(color: state.accentColor),
                                )
                              ],
                            ),
                          ),
                        ),
                      ],
                    ),
                  )),
        );
      },
    );
  }
}
