import 'package:flutter/cupertino.dart';

/// The data, that determines the appearance of the [ClockScreen].
class ClockScreenState {
  final String time;
  final String sleepLeftMessage;
  final Color backgroundColor;
  final Color accentColor;

  ClockScreenState(this.time, this.sleepLeftMessage, this.backgroundColor, this.accentColor);

  @override
  bool operator ==(Object other) {
    if (identical(this, other)) return true;
  
    return other is ClockScreenState &&
      other.time == time &&
      other.sleepLeftMessage == sleepLeftMessage &&
      other.backgroundColor == backgroundColor &&
      other.accentColor == accentColor;
  }

  @override
  int get hashCode {
    return time.hashCode ^
      sleepLeftMessage.hashCode ^
      backgroundColor.hashCode ^
      accentColor.hashCode;
  }
}
