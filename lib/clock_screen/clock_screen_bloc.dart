import 'dart:async';

import 'package:device_display_brightness/device_display_brightness.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:horolux/clock_screen/clock_screen_state.dart';
import 'package:horolux/presets/hl_colors.dart';
import 'package:horolux/settings/settings.dart';

/// Automatically emits [ClockScreenState] periodically, which keeps [ClockScreen] up to date.
class ClockScreenBloc extends Cubit<ClockScreenState> {
  /// The length of the time for which the display increases the brighness.
  final Duration lightDuration;
  // The single point in time, when the user is going to wake up next.
  final DateTime wakeupTime;
  final BuildContext context;

  ClockScreenBloc._default(this.lightDuration, this.wakeupTime, this.context)
      : super(
          genState(wakeupTime, lightDuration, DateTime.now(), context),
        ) {
    _update();
  }

  factory ClockScreenBloc(Settings settings, BuildContext localizations) {
    // I use a factory, so that this can be calculated only once.
    var ndt = _nextDateTime(settings.wakeupTime);
    return ClockScreenBloc._default(settings.lightDuration, ndt, localizations);
  }

  /// Converts the [TimeOfDay] user selected (6:00) to the single 'next wakeup' [DateTime] (6:00 tomorrow morning).
  static DateTime _nextDateTime(TimeOfDay timeOfDay) {
    var now = DateTime.now();
    DateTime nextDay = now;
    // Tests if the selected time already happened today.
    if (nextDay.hour > timeOfDay.hour ||
        (nextDay.hour == timeOfDay.hour &&
            nextDay.minute >= timeOfDay.minute)) {
      // If so, we want to wake up tomorrow.
      nextDay = nextDay.add(const Duration(days: 1));
    }

    var res = DateTime(
      nextDay.year,
      nextDay.month,
      nextDay.day,
      timeOfDay.hour,
      timeOfDay.minute,
    );

    if (!res.isAfter(now)) {
      // The app should crash in this case.
      throw Exception('Time of wake up is not in the future!');
    }

    return res;
  }

  // Has the screen brightness already been set to maximum?
  bool _alreadyIncreasedBrightness = false;

  /// Refreshes the current state and makes sure it gets called again at the time of need.
  void _update() {
    if (super.isClosed) {
      return;
    }
    var now = DateTime.now();
    // TODO Introduce variable update frequency. No need to update every second of the night.
    // ignore: unused_local_variable
    var _timer = Timer(
        const Duration(seconds: 1) - Duration(milliseconds: now.millisecond),
        _update);
    // The only place, where I call emit, so it should be thread safe.
    if (!_alreadyIncreasedBrightness &&
        wakeupTime.difference(now) <= lightDuration) {
      DeviceDisplayBrightness.setBrightness(1.0);
      _alreadyIncreasedBrightness = true;
    }
    emit(genState(wakeupTime, lightDuration, now, context));
  }

  /// Creates the [ClockScreenState].
  @visibleForTesting
  static ClockScreenState genState(
      DateTime wakeupTime, Duration lightDuration, DateTime currentTime,
      [BuildContext? context]) {
    // The time as displayed to the user.
    String timeString;
    if (context == null) {
      timeString = const DefaultMaterialLocalizations()
          .formatTimeOfDay(TimeOfDay.fromDateTime(currentTime));
    } else {
      timeString = TimeOfDay.fromDateTime(currentTime).format(context);
    }

    Duration sleepLeft = wakeupTime.difference(currentTime);
    String sleepLeftMsg = '';
    sleepLeftMsg +=
        (sleepLeft.inHours > 0 ? '${sleepLeft.inHours} hours ' : '');
    sleepLeftMsg += (sleepLeft.isNegative
        ? 'Time to wake up!'
        : '${sleepLeft.inMinutes % 60} minutes left');

    Color background, text;
    if (sleepLeft.isNegative || sleepLeft == Duration.zero) {
      // You are supposed to be up.
      background = HLColors.secondary;
      text = HLColors.primary;
    } else if (sleepLeft < lightDuration && lightDuration != Duration.zero) {
      // The light period has begun.
      var progress =
          1 - (sleepLeft.inMilliseconds / lightDuration.inMilliseconds);
      background = Color.lerp(HLColors.primary, HLColors.secondary, progress) ??
          Colors.red;
      text = progress > 0.5 ? HLColors.primary : HLColors.secondary;
    } else {
      // Keep on sleeping.
      background = HLColors.primary;
      text = HLColors.sleepFont;
    }
    return ClockScreenState(timeString, sleepLeftMsg, background, text);
  }
}
