import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:horolux/presets/hl_colors.dart';
import 'package:horolux/settings/settings_screen.dart';
import 'package:horolux/settings/settings_screen_bloc.dart';

void main() {
  runApp(const MyApp());
}

/// Here is the root of the app. No complex routing is implemented.
class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BlocProvider(
        create: (_) => SettingsScreenBloc(),
        child: MaterialApp(
          theme: HLColors.materialTheme,
          home: const SettingsScreen(),
        ));
  }
}
