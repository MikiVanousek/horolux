import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:horolux/settings/settings.dart';
import 'package:shared_preferences/shared_preferences.dart';

/// Retrieves from and stores in [SharedPreferences] the user [Settings]. Tells the UI what user settings to display.
class SettingsScreenBloc extends Cubit<Settings?> {
  static const sharedPreferencesKey = 'HL_SETTINGS';
  static const defaultSettings =
      Settings(TimeOfDay(hour: 7, minute: 0), Duration(minutes: 30), true);
  late SharedPreferences sharedPreferences;
  SettingsScreenBloc() : super(defaultSettings) {
    _querySharedPreferences();
  }

  void _querySharedPreferences() async {
    sharedPreferences = await SharedPreferences.getInstance();
    var ss = sharedPreferences.getString(sharedPreferencesKey);
    if (ss != null) {
      try {
        emit(Settings.fromJson(ss));
        return;
      } catch (_) {
        debugPrint(
            'The json representation of the settings saved in shared preferences is invalid!\n$ss');
      }
    }
    emit(defaultSettings);
  }

  Future<bool> updateSettings(Settings newSettings) async {
    await sharedPreferences.setString(
        sharedPreferencesKey, newSettings.toJson());
    emit(newSettings);
    return true;
  }
}
