import 'dart:convert';

import 'package:flutter/material.dart';

/// Complete description of the way user wants the app to behave.
class Settings {
  /// The hour and minute the user wakes up at every morning.
  final TimeOfDay wakeupTime;

  /// The length of the time for which the display increases the brighness.
  final Duration lightDuration;

  /// Wether or not the time should be visible on the clock screen.
  final bool showTime;

  const Settings(this.wakeupTime, this.lightDuration, this.showTime);

  Settings copyWith({
    TimeOfDay? wakeupTime,
    Duration? lightDuration,
    bool? showTime,
  }) {
    return Settings(
      wakeupTime ?? this.wakeupTime,
      lightDuration ?? this.lightDuration,
      showTime ?? this.showTime,
    );
  }

  // These functions are used to encode the settings as string and store them in shared preferences.
  Map<String, dynamic> toMap() {
    return {
      'wakeupTime': '${wakeupTime.hour}:${wakeupTime.minute}',
      'lightDuration': lightDuration.inSeconds.toString(),
      'showTime': showTime,
    };
  }

  factory Settings.fromMap(Map<String, dynamic> map) {
    var wakeupTimeRaw = map['wakeupTime']?.split(':');
    var lightDurationString = map['lightDuration'];
    var showTime = map['showTime'];
    if (lightDurationString == null ||
        wakeupTimeRaw == null ||
        wakeupTimeRaw.length != 2) {
      throw Exception('Illegal argument!');
    }
    var wakeupTime = TimeOfDay(
        hour: int.parse(wakeupTimeRaw[0]), minute: int.parse(wakeupTimeRaw[1]));
    var lightDuration = Duration(seconds: int.parse(lightDurationString));
    return Settings(
      wakeupTime,
      lightDuration,
      showTime,
    );
  }

  String toJson() => json.encode(toMap());

  factory Settings.fromJson(String source) =>
      Settings.fromMap(json.decode(source));

  @override
  bool operator ==(Object other) {
    if (identical(this, other)) return true;

    return other is Settings &&
        other.wakeupTime == wakeupTime &&
        other.lightDuration == lightDuration &&
        other.showTime == showTime;
  }

  @override
  int get hashCode =>
      wakeupTime.hashCode ^ lightDuration.hashCode ^ showTime.hashCode;
}
