import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:horolux/clock_screen/clock_screen.dart';
import 'package:horolux/presets/hl_colors.dart';
import 'package:horolux/presets/hl_text_styles.dart';
import 'package:horolux/settings/settings.dart';
import 'package:horolux/settings/settings_screen_bloc.dart';

/// The first screen user sees after opening the app.
/// It allows him to choose when he wakes up and how long before that should
/// the brightness of the screen start increasing.
class SettingsScreen extends StatelessWidget {
  static const avalibleDurations = [
    3,
    5,
    10,
    15,
    20,
    25,
    30,
    40,
    50,
    60,
    80,
    100,
    120
  ];
  const SettingsScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      color: HLColors.primary,
      child: Column(
        mainAxisSize: MainAxisSize.max,
        children: [
          Expanded(
            child: SingleChildScrollView(
              child: BlocBuilder<SettingsScreenBloc, Settings?>(
                builder: (context, settings) {
                  if (settings == null) {
                    return Container();
                  }
                  return Container(
                    width: 600,
                    padding: const EdgeInsets.symmetric(horizontal: 32),
                    child: IntrinsicHeight(
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          const SizedBox(
                            height: 64,
                          ),
                          Text(
                            'Wakeup time:',
                            style: HLTextstyles.label,
                          ),
                          GestureDetector(
                            onTap: () => changeTime(context, settings),
                            child: Text(
                              settings.wakeupTime.format(context),
                              style: HLTextstyles.display,
                            ),
                          ),
                          const SizedBox(
                            height: 32,
                          ),
                          Text(
                            'Light duration:',
                            style: HLTextstyles.label,
                          ),
                          SizedBox(
                            height: 100,
                            child: GestureDetector(
                              onTap: () => changeDuration(context, settings),
                              child: Row(
                                crossAxisAlignment: CrossAxisAlignment.baseline,
                                textBaseline: TextBaseline.ideographic,
                                children: [
                                  Text(
                                    settings.lightDuration.inMinutes.toString(),
                                    style: HLTextstyles.display,
                                  ),
                                  Text(
                                    ' minutes',
                                    style: HLTextstyles.labelAccent,
                                  )
                                ],
                              ),
                            ),
                          ),
                          const SizedBox(
                            height: 32,
                          ),
                          GestureDetector(
                            onTap: () =>
                                BlocProvider.of<SettingsScreenBloc>(context)
                                    .updateSettings(settings.copyWith(
                                        showTime: !settings.showTime)),
                            child: SizedBox(
                              height: 100,
                              child: Row(
                                crossAxisAlignment: CrossAxisAlignment.baseline,
                                mainAxisAlignment:
                                    MainAxisAlignment.spaceBetween,
                                textBaseline: TextBaseline.ideographic,
                                children: [
                                  Text(
                                    settings.showTime
                                        ? 'Show time'
                                        : 'Do not show time',
                                    style: HLTextstyles.labelAccent,
                                  ),
                                  Material(
                                    color: HLColors.primary,
                                    child: Switch(
                                      activeColor: HLColors.secondary,
                                      value: settings.showTime,
                                      onChanged: (value) => BlocProvider.of<
                                              SettingsScreenBloc>(context)
                                          .updateSettings(settings.copyWith(
                                              showTime: !settings.showTime)),
                                    ),
                                  )
                                ],
                              ),
                            ),
                          ),
                        ],
                      ),
                    ),
                  );
                },
              ),
            ),
          ),
          const SizedBox(
            height: 32,
          ),
          Container(
            width: 600,
            padding: const EdgeInsets.symmetric(horizontal: 32),
            child: GestureDetector(
              onTap: () => Navigator.of(context)
                  .push(MaterialPageRoute(builder: (_) => const ClockScreen())),
              child: Container(
                height: 64,
                decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(12),
                    color: HLColors.secondary),
                child: Center(
                    child: Text(
                  'Start',
                  style: HLTextstyles.button,
                )),
              ),
            ),
          ),
          const SizedBox(
            height: 32,
          ),
        ],
      ),
    );
  }

  void changeTime(BuildContext context, Settings state) async {
    var selectedTime = await showTimePicker(
      initialTime: state.wakeupTime,
      context: context,
    );
    if (selectedTime != null) {
      var bloc = BlocProvider.of<SettingsScreenBloc>(context);
      bloc.updateSettings(state.copyWith(wakeupTime: selectedTime));
    }
  }

  void changeDuration(BuildContext context, Settings state) async {
    List<SimpleDialogOption> options = [];
    final bloc = BlocProvider.of<SettingsScreenBloc>(context);
    for (var i in avalibleDurations) {
      options.add(
        SimpleDialogOption(
          child: Text(i.toString() + ' minutes',
              style: state.lightDuration.inMinutes == i
                  ? HLTextstyles.durationAccent
                  : HLTextstyles.duration),
          onPressed: () {
            bloc.updateSettings(
                state.copyWith(lightDuration: Duration(minutes: i)));
            Navigator.of(context).pop();
          },
        ),
      );
    }
    showDialog(
        context: context,
        builder: (_) => SimpleDialog(
              backgroundColor: HLColors.primaryLight,
              children: options,
            ));
  }
}
